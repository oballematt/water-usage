const express = require('express');
const app = express();

app.use(express.static('public'));

app.get('/', function (req, res) {
  res.render('index.html');
})

app.listen(80, function () {
  console.log('listening on port 80')
})
