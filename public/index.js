var search = decodeURI(location.search);
var building_names = JSON.parse(search.split("=")[1]);
var outside_fire_wall_access = false;
var month_range = 13;
var day_offset = 2;
var data1;
var data2;
var seriesCounterBar = 0;
var seriesOptions = [];
var building_data;

function create_bar_chart() {
    Highcharts.stockChart('container', {
        chart: {
            type: 'column',
            height: (5 / 10 * 100) + '%'
        },

        credits: {
            enabled: false
        },

        xAxis: {
            crosshair: true,
            labels: {
                formatter: function(){
                    var d0 = new Date(this.value);
                    var m = d0.getMonth();

                    var month = new Array();
                    month[0] = "Jan";
                    month[1] = "Feb";
                    month[2] = "Mar";
                    month[3] = "Apr";
                    month[4] = "May";
                    month[5] = "Jun";
                    month[6] = "Jul";
                    month[7] = "Aug";
                    month[8] = "Sep";
                    month[9] = "Oct";
                    month[10] = "Nov";
                    month[11] = "Dec";
                    // console.log(d0, m, month[m]);

                    return month[(m+1)%12];
                }
            }
        },

        rangeSelector: {
            inputEnabled: false,
            selected: 3,
            buttons: [{
                type: 'month',
                count: 3,
                text: '3m'
            }, {
                type: 'month',
                count: 6,
                text: '6m'
            }, {
                type: 'ytd',
                text: 'YTD'
            }, {
                type: 'year',
                count: 1,
                text: '1y'
            }, {
                type: 'all',
                text: 'All'
            }]
        },

        yAxis: {
            min: 0,
            title: {
                text: 'Gallons'
            },
            crosshair: true
        },

        title:{
            text: "Domestic Water Use"
        },

        legend: {
            enabled: true
        },

        plotOptions: {
            series: {
                showInNavigator: true
            }
        },

        tooltip: {
            formatter: function () {
                // The first returned item is the header, subsequent items are the points

                var month = new Array();
                month[0] = "Jan";
                month[1] = "Feb";
                month[2] = "Mar";
                month[3] = "Apr";
                month[4] = "May";
                month[5] = "Jun";
                month[6] = "Jul";
                month[7] = "Aug";
                month[8] = "Sep";
                month[9] = "Oct";
                month[10] = "Nov";
                month[11] = "Dec";
                var create_tooltip = function(point){
                    return point.series.name + ': ' + point.y.toLocaleString() + '<br/>';
                };
                var array_of_tooltips = this.points ? this.points.map(create_tooltip) : [];
                return ['<b>' + month[new Date(this.x).getMonth()] + '</b><br/>'].concat(array_of_tooltips);
            },
            split: false,
            shared: true
        },

        series: seriesOptions
    });
}

function convert_to_csv(){
    var buildings = Object.keys(building_data);
    var header = "Time, " + buildings.join(", ");
    var csv_data = header + "\n";
    var n_lines = building_data[buildings[0]].length;
    for(i=0; i!=n_lines; i++){
        var time = building_data[buildings[0]][i].T;
        var line = (new Date(time)).toLocaleDateString();
        for(building in building_data){
            line = line + ", " + building_data[building][i].V;
        }
        csv_data = csv_data + line + "\n";
    }
    const csvContent = "data:text/csv;charset=utf-8," + csv_data;
    window.open(encodeURI(csvContent));
}

function process(){
    building_data = data1.data;
    for(var building_name in building_data){
        var data = building_data[building_name];
        var points = [];
        
        data.forEach(function(prism_point){
            console.log(new Date(prism_point.T), prism_point.V);
            var d0 = new Date(prism_point.T);
            var year = d0.getFullYear();
            var month = d0.getMonth();
            year = Math.floor(((year*12 + month) - 1) / 12);
            month = ((year*12 + month) - 1) % 12;
            d0 = new Date(year,month,1);
            prism_point.T = d0.getTime();
            var point = [prism_point.T, prism_point.V];
            points.push(point);
        })

        building_name = building_name.split("_")[0];
        seriesOptions[seriesCounterBar] = {
            name: building_name,
            data: points
        };

        seriesCounterBar += 1;
        if (seriesCounterBar === building_names.length) {
            console.log("creating chart");
            $("#loader").hide();
            create_bar_chart();
        }
    }
}

function combining_data(data1, building){
    var final_point = data2.data[building][0];
    data1.data[building] = data1.data[building].concat(final_point);
}

function combine(data1, data2){
    var buildings = Object.keys(data1.data);
    buildings.forEach(function(building){
        combining_data(data1, building);
    })
}

function main(){
    debugger;
    // var p1 = JSON.parse(search.split("?")[1].split("&")[0].split("=")[1]);
    // var p2 = JSON.parse(search.split("?")[1].split("&")[1].split("=")[1]);
    // var dummy_data = JSON.parse("[{\"T\":1570338000000,\"V\":16900,\"Q\":195},{\"T\":1572930000000,\"V\":20300,\"Q\":195},{\"T\":1575522000000,\"V\":19900,\"Q\":195},{\"T\":1578114000000,\"V\":17400,\"Q\":195},{\"T\":1580706000000,\"V\":21600,\"Q\":195},{\"T\":1583298000000,\"V\":24700,\"Q\":195},{\"T\":1585890000000,\"V\":9600,\"Q\":195},{\"T\":1588482000000,\"V\":12300,\"Q\":195},{\"T\":1591074000000,\"V\":4000,\"Q\":195},{\"T\":1593666000000,\"V\":2700,\"Q\":195},{\"T\":1596258000000,\"V\":2700,\"Q\":195},{\"T\":1598850000000,\"V\":3500,\"Q\":195},{\"T\":1601442000000,\"V\":5800,\"Q\":195},{\"T\":1604034000000,\"V\":7600,\"Q\":195}]");

    var to = new Date();
    var to_year = to.getFullYear();
    var to_month = to.getMonth();
    var to_api = new Date(to_year, to_month, day_offset);
    var to_month_serial = to_year*12 + to_month;

    var ex_month_serial = to_year*12 + to_month + 1;
    var ex_year = Math.floor(ex_month_serial / 12);
    var ex_month = ex_month_serial % 12;
    var ex_api = new Date(ex_year, ex_month, day_offset);

    var fr_year = Math.floor((to_month_serial - month_range) / 12);
    var fr_month = (to_month_serial - month_range) % 12;
    var fr_api = new Date(fr_year, fr_month, day_offset);

    var seriesCounterLine = 0;
    var host;
    if(outside_fire_wall_access){
        host = "energyportal.utilities.utexas.edu";
    } else {
        host = "10.101.206.21";
    }
    var tags = [];
    building_names.forEach(function(building_name){
        var tag = building_name + "_W_TBU_PM";
        tags.push(tag);
    })
    console.log(fr_api, to_api);
    var url1 = "https://"+host+"/webapi/api/trend/multitag?tags="+tags+"&start="+fr_api.getTime()+"&end="+to_api.getTime()+"&interval="+31*24*60*60*1000;
    var url2 = "https://"+host+"/webapi/api/trend/multitag?tags="+tags+"&start="+to_api.getTime()+"&end="+ex_api.getTime()+"&interval="+31*24*60*60*1000;
    // url = "https://10.101.206.21/webapi/api/trend/tag?tag="+tag+"&start="+$from.to_s+"&end="+$to.to_s+"&interval=3600000"
    console.log(url1);
    console.log(url2);

    Highcharts.getJSON(
        url1,
        function(data) {
            data1 = data;
            Highcharts.getJSON(
                url2,
                function(data){
                    data2 = data;
                    combine(data1, data2);
                    process();
                }
            );

        }
    );

    $("#go").click(function(){
        var new_url = "parameters.html";
        var new_url = encodeURI(new_url); 

        location.href = new_url;
    });

    $("#export").click(function(){
        convert_to_csv();
    });
};

$(document).ready(function(){
    setTimeout(main, 1000);
});